import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation
import matplotlib.cm as cm
import sys
import math
import copy

#Distanve From Aperature
z = 0.0
wavelength = 532E-9 #nm (x10-9??)
#Quantities chosen to allow scaling factor to be one

#aperture = #A shape. 2D

starting_point = -3
time_to_plot = 6 # seconds

wavenumber = (2 * np.pi) / wavelength

def rect(t, length=0.1):
	if abs(t) <= length:
		return 1
	elif abs(t) > length:
		return 0

def rect2D(x,y):
	return rect(x)*rect(y)

#x**2 + y**2 = r**2
def circ(x,y,radius=0.01):
	if ((x**2 + y**2) < radius):
		return 1
	else:
		return 0

def euclideanDistance(x1, y1, x2, y2):
	global z
	return math.sqrt( (x2 - x1)**2 + (y2 - y1)**2 + (z)**2)

def G(x,y):
	return (np.exp(1j*k*z) * np.exp(1j*(k / (2*z) ) * (x**2 + y**2)))/(1j * wavelength * z) * np.fft.fft2(aperture)

#Scaling function that is essentially a low-pass filter that clips at 10% of max value
#I also added on the log of the actual high values to try not to throw away the information entirely but it doesn't seem to make a huge difference
def scale(list_2D, factor=10):
	maxVal = max(list_2D[len(list_2D)//2])
	for i,row in enumerate(list_2D):
		list_2D[i] = [x if (x < maxVal/factor) else (maxVal/factor + np.log10(x)) for x in row]
	return list_2D

def rayleighSommerfield(signal):
	global wavenumber
	global wavelength

	out = copy.deepcopy(signal)
	completion = len(signal) * len(signal[0]) #Assuming square input
	
	for i in range(len(out)):
		for j in range(len(out[i])):
			#Algorithm is crazy inefficient so I'm only looking at +/-10% away from the middle in either dimension
			for k in range((len(signal)//2) - (len(signal)//10), (len(signal)//2) + (len(signal)//10)):
				for l in range((len(signal[k])//2) - (len(signal[k])//10), (len(signal[k])//2) + (len(signal[k])//10)):
					dist = euclideanDistance(i, j, k, l)
					dist += (dist*0.2)
					#If point is in aperture
					if(signal[k][l] == 0):
						continue;
					elif (dist == 0):
						out[i][j] = signal[k][l]
					else:
						out[i][j] += (signal[k][l]/(1j*wavelength)) *  (np.exp(1j * wavenumber * dist) / dist) * np.cos(np.arctan((l - j)/((k - i) + 0.000000001)))

		print ("{0}% Complete".format( round(((i*j) / completion) * 100, 2)))

	return out

def calc_power_spectra(signal, title):
	#My attempt at 3D plots, must come back to this
	#fig, axarr = plt.subplots(2,2)
	#axarr[0,0] = fig.gca(projection='3d')

	#X, Y = np.meshgrid(X, Y)

	#axarr[0,0].plot_surface(X, Y, Z)
	
	#plt.show()
	fig, axarr = plt.subplots(2,2)
	fig.suptitle(title, fontsize=20)

	axarr[0,0].set_title("Input Signal")
	axarr[0,0].imshow(signal, cmap=cm.Greys_r)

	diffractedSignal = rayleighSommerfield(signal)

	axarr[0,1].set_title("Diffracted Signal (Rayleigh Sommerfield)")
	axarr[0,1].imshow(np.real(scale(diffractedSignal)), cmap=cm.Greys_r)

	fft_output = np.fft.fft2(signal)
	
	axarr[1,0].set_title("Frequency Domain")
	axarr[1,0].imshow(np.log2(np.real(np.fft.fftshift(fft_output))), cmap=cm.Greys_r)
	
	pow_spec = (np.abs(fft_output))**2
	axarr[1,1].set_title("Power Spectrum")
	axarr[1,1].imshow(scale(np.fft.fftshift((pow_spec))), cmap=cm.Greys_r)

	plt.savefig("{0} Diffraction.png".format(title), dpi=96)
	plt.show()

function = rect2D
title = ""

while True:
	x = np.linspace(starting_point, starting_point + time_to_plot, 3/0.01 + 1)
	y = np.linspace(starting_point, starting_point + time_to_plot, 3/0.01 + 1)

	index = input("Please Choose An Aperture Shape:\
					\n1. Square\
					\n2. Circle\
					\n3. Exit\
					\n\n")

	if index == '1':
		title ="Rectangular Aperture"
		function = rect2D
	elif index == '2':
		function = circ
		title = "Circular Aperture"
	elif index == '3':
		sys.exit(1)
	else:
		print("Invalid Input, Please Choose From the available Options\n")
		continue

	sig = []
	for i,k in enumerate(x):
		sig.append([])
		for j in y:
			sig[i].append(function(k, j))

	calc_power_spectra(sig, title)